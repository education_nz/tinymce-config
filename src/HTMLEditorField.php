<?php

namespace Education\TinyMCEConfig;

use SilverStripe\Forms\HTMLEditor\HTMLEditorField as Original;
use SilverStripe\Forms\HTMLEditor\HTMLEditorConfig;
use SilverStripe\Control\Controller;
use SilverStripe\Core\Manifest\ModuleLoader;

class HTMLEditorField extends Original
{
    /**
     * @var HTMLEditorConfig $builtConfig
     *
     * Cached built configuration.
     */
    private static $builtConfig = null;

    public function __construct($name, $title = null, $value = '', $config = null)
    {
        if (!$config) {
            $config = $this->getEditorConfig();
        }

        parent::__construct($name, $title, $value, $config);
    }

    public function getEditorConfig()
    {
        return $this->getEducationConfig();
    }

    public function getEducationConfig()
    {
        if (self::$builtConfig) {
            return self::$builtConfig;
        }

        $cwpEditor = HTMLEditorConfig::get('cwp');
        $cwpEditor->enablePlugins('compat3x');
        $cwpEditor->enablePlugins('lists');
        $cwpEditor->enablePlugins('hr');
        $cwpEditor->enablePlugins('anchor');

        $blocks = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Address=address;Pre=pre;';
        $cwpEditor->setOption('block_formats', $blocks);

        $themesPath = Controller::join_links(BASE_PATH . '/themes/');
        $templates = [];

        if (file_exists($themesPath)) {
            foreach (scandir("$themesPath/") as $theme) {
                if ($theme == '.' || $theme == '..') {
                    continue;
                }

                $path = Controller::join_links($themesPath, $theme, '/templates/tinymce_pre_defined');

                if (file_exists($path)) {
                    foreach (glob("$path/*.html") as $file) {
                        $fileName = array_slice(explode('/', $file), -1)[0];
                        $title = str_replace('.html', '', $fileName);
                        $description = null; // todo manifest.json for this info

                        $templates[] = [
                            'title' => $title,
                            'url' => 'themes/'. $theme .'/templates/tinymce_pre_defined/'. $fileName,
                            'description' => $description
                        ];
                    }
                }
            }
        }

        if ($templates) {
            $cwpEditor->setOptions(
                [
                'templates' => $templates
                ]
            );
        }

        // allow style_formats to append classes to this list,
        $cwpEditor->setOption('style_formats', []);
        $cwpEditor->setOption('importcss_append', true);

        $cwpEditor->addButtonsToLine(1, 'hr');
        $cwpEditor->addButtonsToLine(1, 'visualchars');
        $cwpEditor->addButtonsToLine(1, 'superscript');
        $cwpEditor->addButtonsToLine(1, 'subscript');
        $cwpEditor->addButtonsToLine(1, 'undo');
        $cwpEditor->addButtonsToLine(1, 'redo');
        $cwpEditor->addButtonsToLine(1, 'blockquote');

        $module = ModuleLoader::inst()->getManifest()->getModule('silverstripe/admin');
        $cwpEditor->enablePlugins(
            [
            'sslink' => $module->getResource('client/dist/js/TinyMCE_sslink.js'),
            'sslinkexternal' => $module->getResource('client/dist/js/TinyMCE_sslink-external.js'),
            'sslinkemail' => $module->getResource('client/dist/js/TinyMCE_sslink-email.js'),
            ]
        )->setOption('contextmenu', 'sslink ssmedia ssembed inserttable | cell row column deletetable');

        $cwpEditor->setOption('contextmenu', 'sslink ssmedia ssembed inserttable | cell row column deletetable');

        self::$builtConfig = $cwpEditor;

        return $cwpEditor;
    }
}
