<?php

namespace Education\TinyMCEConfig\Tests;

use SilverStripe\Dev\SapphireTest;
use SilverStripe\Forms\HTMLEditor\HTMLEditorConfig;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class TinyMCEConfigTest extends SapphireTest
{
    public function testConfigIncludesPlugins()
    {
        $config = HtmlEditorConfig::get('cwp');

        // $this->assertContains('templates', array_keys($config->getPlugins()));
        $this->assertContains('hr', array_keys($config->getPlugins()));
    }

    public function testCwpShouldBeDefaultConfig()
    {
        $field = HTMLEditorField::create('TestField');
        $config = HtmlEditorConfig::get('cwp');

        $this->assertEquals($config, $field->getEditorConfig());
    }
}
